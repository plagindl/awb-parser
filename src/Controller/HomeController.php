<?php

namespace App\Controller;

use PHPHtmlParser\Dom;
use PHPHtmlParser\Exceptions\{
    ChildNotFoundException,
    CircularException,
    CurlException,
    NotLoadedException,
    StrictException};
use PHPHtmlParser\Dom\HtmlNode;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use PhpOffice\PhpSpreadsheet\Writer\Exception;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use RuntimeException;

class HomeController
{
    private Dom $dom;

    protected string $baseUrl = 'https://www.turkishcargo.com.tr/en/online-services/shipment-tracking?quick=true&';

    public function __construct()
    {
        $this->dom = new Dom();
    }

    /**
     * @return mixed
     * @throws ChildNotFoundException
     * @throws CircularException
     * @throws CurlException
     * @throws StrictException
     * @throws NotLoadedException
     */
    public function index()
    {
        $awbNumber = $_GET['awbNumber'] ?? null;

        if (!$awbNumber) {
            return require 'views/app.php';
        }

        $dom = $this->getResourcePage($awbNumber);

        $parsedTables = json_encode($this->extractTableData($dom));

        return require 'views/app.php';
    }

    /**
     * @throws ChildNotFoundException
     * @throws CircularException
     * @throws CurlException
     * @throws StrictException
     * @throws NotLoadedException
     * @throws Exception
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     */
    public function generateXls()
    {
        $awbNumber = $_GET['awbNumber'] ?? null;

        if (!$awbNumber) {
            throw new RuntimeException('No AWB Number given.', 400);
        }

        $dom = $this->getResourcePage($awbNumber);
        $tables = $this->extractTableData($dom);
        $spreadsheet = new Spreadsheet();
        $spreadsheet->getActiveSheet()->fromArray($tables[0]);

        for ($i = 1; $i < count($tables); $i++) {
            $table = $tables[$i];

            $spreadsheet->addSheet((new Worksheet())->fromArray($table)->setTitle('Worksheet ' . ($i + 1)), $i);
        }

        $filePath = '/public/files/' . $awbNumber.'.xlsx';;

        $writer = new Xlsx($spreadsheet);
        $writer->save('.' . $filePath);

        header("Location: " . $filePath);
        exit();
    }

    /**
     * @param string $awbNumber
     * @return Dom
     * @throws ChildNotFoundException
     * @throws CircularException
     * @throws CurlException
     * @throws StrictException
     */
    protected function getResourcePage(string $awbNumber): Dom
    {
        return $this->dom->loadFromUrl($this->baseUrl . 'awbInput=' . $awbNumber);
    }

    /**
     * @param Dom $dom
     * @return array|HtmlNode
     * @throws ChildNotFoundException
     * @throws NotLoadedException
     */
    protected function extractTableData(Dom $dom): array
    {
        $tables = [];

        foreach ($dom->find('table') as $table) {
            $tableData = [];
            $tableHeaders = $table->find('th');

            if ($tableHeaders->count()) {
                $tableData['head'] = array_map(
                    fn(HtmlNode $headDivision) => $headDivision->text(),
                    $tableHeaders->toArray()
                );
            }

            $tableRows = $table->find('tr');

            /** @var HtmlNode $row */
            foreach ($tableRows as $tableRow) {
                $divisions = $tableRow->find('td');

                if (!$divisions->count()) {
                    continue;
                }

                $row = [];

                /** @var HtmlNode $division */
                foreach ($divisions as $division) {
                    if ($division->find('input[type=checkbox]')->count()) {
                        $checkedAttribute = $division->find('input[type=checkbox]')[0]->getAttribute("checked");
                        $row[] = $checkedAttribute === '&quot;checked&quot;';

                        continue;
                    }

                    $row[] = $division->text();
                }

                $tableData[] = $row;
            }

            $tables[] = $tableData;
        }

        return $tables;
    }
}