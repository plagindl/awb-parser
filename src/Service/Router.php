<?php

namespace App\Service;

use Exception;
use RuntimeException;

class Router
{
    protected array $routes = [];

    public function __construct(array $routes)
    {
        $this->routes = $routes;
    }

    /**
     * @param $uri
     * @throws Exception
     */
    public function direct($uri)
    {
        if (!array_key_exists($uri, $this->routes)) {
            throw new RuntimeException('No routes found mathing '. $uri, 404);
        }

        [$controllerName, $action] = explode('@', $this->routes[$uri]);

        $controllerClass = '\App\Controller\\' . $controllerName;

        if (!class_exists($controllerClass)) {
            throw new Exception('Controller ' . $controllerClass . ' does not exist');
        }

        $controller = new $controllerClass;

        if (!method_exists($controller, $action)) {
            throw new Exception('Method ' . $action . ' does not exist in ' . $controllerClass);
        }

        $controller->$action();
    }
}
