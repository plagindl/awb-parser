<?php

require __DIR__ . '/vendor/autoload.php';

require __DIR__ . '/public/bootstrap.php';

$url = parse_url($_SERVER['REQUEST_URI']);

$router->direct(trim($url['path'], '/'));