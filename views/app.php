<!DOCTYPE html>
<html lang="en">
<head>
	<title>Turkish Cargo Parser</title>
</head>
<body>
	<form method="GET">
        <label for="awbNumberInput"></label>
        <input id="awbNumberInput"
               type="text"
               name="awbNumber"
               placeholder="AWB Number"
               value="<?php echo $_GET['awbNumber'] ?? null ?>"
        >

        <input type="submit" value="Find">
    </form>

	<?php if (isset($parsedTables)): ?>
        <p>// JSON</p>
        <?php echo $parsedTables ?>

        <form target="_blank" method="GET" action="/generate-xls">
            <input type="hidden" name="awbNumber" value="<?php echo $_GET['awbNumber'] ?>">
            <input type="submit" value="Generate XLS">
        </form>
	<?php endif; ?>
</body>
</html>