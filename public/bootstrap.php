<?php

use App\Service\Router;

$routes = require dirname(__DIR__) . '/src/routes.php';

$router = new Router($routes);
